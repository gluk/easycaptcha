let
    rp = require('request-promise'),
    debug = require('debug')('easycaptcha');
    //base64Img = require('base64-img');


class easyCaptcha {
    constructor(config) {
        if (config.proxy) {
            rp = rp.defaults({'proxy': config.proxy});
        }
        this.console = config.console || false;
        this.loopKey = null;
        this.timeoutKey = null;
        this.timeout = config.timeout || 180000;
        this.interval = config.interval || 5000;
        this.softId = 1570;
        this.reqUri = 'http://rucaptcha.com/in.php';
        this.resUri = 'http://rucaptcha.com/res.php';
        this.key = config.key;
        this.method = config.method; // userrecaptcha
//        this.json = 1;
        this.warning = ['CAPCHA_NOT_READY'];
        this.errors = {
            repeat: [
                'ERROR_NO_SLOT_AVAILABLE'
            ],
            stop: [
                'ERROR_CAPTCHA_UNSOLVABLE',
                'ERROR_WRONG_USER_KEY',
                'ERROR_KEY_DOES_NOT_EXIST',
                'ERROR_ZERO_BALANCE',
                'ERROR_ZERO_CAPTCHA_FILESIZE',
                'ERROR_TOO_BIG_CAPTCHA_FILESIZE',
                'ERROR_WRONG_FILE_EXTENSION',
                'ERROR_IMAGE_TYPE_NOT_SUPPORTED',
                'ERROR_IP_NOT_ALLOWED',
                'IP_BANNED',
                'ERROR_CAPTCHAIMAGE_BLOCKED'
            ]
        }
    }

    urlToBase64(url) {
        return rp.get({
            url: url,
            resolveWithFullResponse: true,
            encoding: null
        }).then(response =>'data:' + response.headers['content-type'] + ';base64,' + Buffer.from(response.body).toString('base64'));
    }



    validateResponse(response) {
        if (response.indexOf('OK|') != -1) {
            let id = response.split('|')[1];
            return {status: 'OK', response: id};
        }
        if (this.warning.indexOf(response) != -1) {
            return {status: 'wait', response: response};
        } else if (this.errors.repeat.indexOf(response) != -1) {
            return {status: 'repeat', response: response};
        } else if (this.errors.stop.indexOf(response) != -1) {
            return {status: 'stop', response: response};
        } else {
            return {status: 'uncnown', response: response};
        }
    }

    requestBalamce() {
        return new Promise((resolve, reject) => {
            let options = {
                resolveWithFullResponse: true,
                uri: this.resUri,
                method: 'GET',
                qs: {
                    soft_id: this.softId,
                    key: this.key,
                    action: 'getBalance'
                },
                headers: {
                    'User-Agent': 'Request-Promise for EasyCaptcha'
                },
                json: true
            };
            rp(options)
                .then((response) => {
                    if (response.statusCode == 200) {
                        if (typeof response.body === 'number') {
                            if (this.console)
                                debug('Баланс: %s', response.body);
                            resolve(response.body);
                        } else {
                            if (this.validateResponse(response.body).status === 'uncnown') {
                                if (this.console)
                                    debug('Нет ошибки в рукаптча, случилось неожиданное');
                                reject(new Error('ERR_EC_UNCNOWN_RESPONSE'));
                            } else {
                                if (this.console)
                                    debug('Получена ошибка от ruCaptcha: %s', response.body);
                                reject(new Error(response.body));
                            }
                        }
                    } else {
                        if (this.console)
                            debug('Получен некорректный ответ по статус коду, код: %s', response.statusCode);
                        reject(new Error('ERR_EC_CODE' + response.statusCode));
                    }

                })
                .catch(error => {
                    error = Object.assign({}, {
                        name: error.name,
                        statusCode: error.statusCode,
                        message: error.message,
                        error: error.error
                    });
                    reject(error);
                });

        });

    }

    requestReCaptcha(config) {
        let googlekey = config.googlekey;
        let proxy = config.proxy;
        let proxytype = config.proxytype;
        let pageurl = config.pageurl;
        return new Promise((resolve, reject) => {
                debug('Запущен процесс EasyCaptcha:requestReCaptcha');
                let options = {
                    uri: this.reqUri,
                    method: 'GET',
                    qs: {
                        soft_id: this.softId,
                        key: this.key,
                        method: this.method,
                        googlekey: googlekey,
                        pageurl: pageurl
                    },
                    headers: {
                        'User-Agent': 'Request-Promise for EasyCaptcha'
                    },
                    json: true
                };
                if (proxy && proxytype) {
                    options.qs.proxy = proxy;
                    options.qs.proxytype = proxytype;
                }
                rp(options)
                    .then((response) => {
                        debug('Получен ответ за запрос расшифровки каптчаб %s', response);
                        if (this.validateResponse(response).status == 'OK') {
                            debug('Закончен процесс EasyCaptcha:requestReCaptcha');
                            resolve(this.validateResponse(response).response);
                        } else {
                            if (this.validateResponse(response).status !== 'uncnown') {
                                debug('Вовзращена ошибка рупатча %s', response);
                                reject(this.validateResponse(response));
                            } else {
                                debug('Вовзращена неизвестная ошибка %s', response);
                                reject(this.validateResponse(response));
                            }

                        }
                    })
                    .catch(error => {
                        debug('Произошла ошибка в процессе EasyCaptcha:requestReCaptcha: %j', JSON.stringify(error));
                        reject(error);
                    });
            }
        );
    }

    checkAllCaptcha(id) {
        return new Promise((resolve, reject) => {
            let self = this;
            let options = {
                uri: this.resUri,
                method: 'GET',
                qs: {
                    key: this.key,
                    action: 'get',
                    id: id
                },
                headers: {
                    'User-Agent': 'Request-Promise for EasyCaptcha'
                },
                json: true
            };

            function loopRp() {
                rp(options)
                    .then(function (response) {
                        let validResponse = self.validateResponse(response);
                        if (validResponse.status == 'OK') {
                            clearTimeout(self.timeoutKey);
                            resolve(validResponse.response);
                        } else {
                            debug('В loop вовзращена ошибка %s', response);
                            if (validResponse.status == 'stop' || validResponse.status == 'uncnown') {
                                clearTimeout(self.timeoutKey);
                                reject(new Error(response));
                            } else {
                                self.loopKey = setTimeout(loopRp, self.interval);
                            }
                        }
                    })
                    .catch(error => {
                        debug('Произошла ошибка в процессе EasyCaptcha:checkAllCaptcha %j', JSON.stringify(error));
                        clearTimeout(self.timeoutKey);
                        reject(error);
                    });
            }

            this.timeoutKey = setTimeout((error, loopKey) => {
                if (loopKey) {
                    clearTimeout(loopKey);
                    reject(error);
                }
            }, this.timeout, new Error('EC_ERR_TIMEOUT'), this.loopKey);
            this.loopKey = setTimeout(loopRp, 5000);
        });
    }

    reCaptcha(config) {
        return new Promise((resolve, reject) => {
            this.requestReCaptcha(config)
                .then(id => {
                    this.checkAllCaptcha(id)
                        .then(response => {
                            resolve(response);
                        }).catch(error => {

                        debug('Произошла ошибка в EasyCaptcha:reCaptcha:checkAllCaptcha %j', JSON.stringify(error));
                        reject(error);
                    })
                }).catch(error => {
                debug('Произошла ошибка в EasyCaptcha:reCaptcha:requestReCaptcha %j', JSON.stringify(error));
                reject(error);
            });
        })
    }

    // requestCaptchaHref(href, params) {
    //     return new Promise((resolve, reject) => {
    //         base64Img.requestBase64(href, (err, res, body) => {
    //             if (err) reject(err);
    //             this.requestCaptcha(body, params)
    //                 .then(resolve)
    //                 .catch(reject);
    //         });
    //     })
    // }
    //
    requestCaptchaHref(href, params) {
        return this.urlToBase64(href)
            .then(base64body => {
                return this.requestCaptcha(base64body, params)
            });
    }

    requestCaptcha(base64img, params) {
        return new Promise((resolve, reject) => {
                debug('Запущен процесс EasyCaptcha:requestCaptcha');
                let form = {
                    soft_id: this.softId,
                    key: this.key,
                    method: this.method,
                    body: base64img
                };
                if (params instanceof Object) {
                    form = Object.assign(form, params);
                }

                let options = {
                    uri: this.reqUri,
                    method: 'POST',
                    form: form,
                    headers: {
                        'User-Agent': 'Request-Promise for EasyCaptcha',
                        'content-type': 'application/x-www-form-urlencoded'
                    },
                };

                rp(options)
                    .then((response) => {
                        debug('Получен ответ в EasyCaptcha:requestCaptcha %s', response);
                        if (this.validateResponse(response).status == 'OK') {
                            debug('Закончена работа EasyCaptcha:requestCaptcha');
                            resolve(this.validateResponse(response).response);
                        } else {
                            debug('Ообнаружена ошибка в ответе EasyCaptcha:requestCaptcha %s', response);
                            reject(this.validateResponse(response));
                        }
                    })
                    .catch(error => {
                        debug('Произошла ошибка в EasyCaptcha:requestCaptcha %j', JSON.stringify(error));
                        reject(error);
                    });
            }
        );

    }

    captcha(img, params) {
        return new Promise((resolve, reject) => {
            if (img.includes('base64')) {
                this.requestCaptcha(img, params || {})
                    .then(id => {
                        this.checkAllCaptcha(id)
                            .then(response => {
                                resolve(response);
                            })
                            .catch(error => {
                                debug('Произошла ошибка в EasyCaptcha:captcha:requestCaptcha:checkAllCaptcha %j', JSON.stringify(error));
                                reject(error);
                            })
                    }).catch(error => {
                    debug('Произошла ошибка в EasyCaptcha:captcha:requestCaptcha %j', JSON.stringify(error));
                    reject(error);
                });
            } else {
                this.requestCaptchaHref(img, params || {})
                    .then(id => {
                        this.checkAllCaptcha(id)
                            .then(response => {
                                resolve(response);
                            })
                            .catch(error => {
                                debug('Произошла ошибка в EasyCaptcha:captcha:requestCaptchaHref:checkAllCaptcha %j', JSON.stringify(error));
                                reject(error);
                            })
                    }).catch(error => {
                    debug('Произошла ошибка в EasyCaptcha:captcha:requestCaptchaHref %j', JSON.stringify(error));
                    reject(error);
                });
            }
        })
    }
}

module.exports = exports = easyCaptcha;
