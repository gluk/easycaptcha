# EasyCaptcha модуль для работы с rucaptcha/2captcha

### подключение
```javascript
const EC = require('easycaptcha');
```
### Инициализация объекта
Для разгардывания картинки:
```javascript
let ec = new EC({
    key: key,
    method: 'base64',
    interval: 1500
});
```
Для разгадывания гугл рекаптча:
```javascript
let ec = new EC({
        key: key,
        method: 'userrecaptcha',
        interval: 1500
    });
```
> key - ключ апи для rucaptcha
> method - base64 или userrecaptcha для разгадывания картинки и гуглрекаптча соответственно
> interval - время переопроса сервера в милисекундах, используется при ожидании ответа для поставленной задачи. Через interval милисекнд происходит переопрос

### Использование объекта в режиме 'base64'

Как промис:
```javascript
ec.captcha(imgsrc, params)
    .then(response=> {
        console.log(response);
    })
    .catch(error => {
        console.error(error);
    });
```
Параметры:
> imgsrc - base64 строка изображения или ссыылка на файл
> params - объект доп настроек rucaptcha, если не требуется передаем пустой объект {}, в следующей версии этот параметр будет как дополнительный и передавать его не потребуется.


В функции-генераторе
````javascript
function *generator(){
    try {
        let response = yield ec.captcha(imgsrc, {});
        }
    catch (error) {
        console.error(error);
        }
}
````
> Аналогично можно использовать в конструкции async await

### Использование объекта в режиме 'userrecaptcha'

Как промис:
```javascript
ec.reCaptcha({
    googlekey: '6Lc_a***************Yf16',
    pageurl: 'http://http.myjino.ru/recaptcha/test-get.php'//,
    //proxy: 'guest:12345@XXX.XXX.XXX.XXX:XXXXX',
    //proxytype: 'HTTP'
})
    .then(response=> {
        console.log('captcha: ', response);
    })
    .catch(error=> {
        console.error(error);
    });
```

В функции-генераторе
```javascript
function *generator(){
    try {
        let response = yield ec.reCaptcha({
                                              googlekey: '6Lc_a***************Yf16',
                                              pageurl: 'http://http.myjino.ru/recaptcha/test-get.php'//,
                                              //proxy: 'guest:12345@XXX.XXX.XXX.XXX:XXXXX',
                                              //proxytype: 'HTTP'
                                          });
        }
    catch (error) {
        console.error(error);
        }
}
```
